import React, { useEffect, useState } from 'react';
import classes from './App.module.css';
import NewsItemList from './components/newsItemList/NewsItemList';
import config from "./configs/NewsConfig";


const TOP_STORIES_URL = config.TOP_STORIES_URL; 
const STORY_URL = config.STORY_URL; 
const AUTHOR_URL = config.AUTHOR_URL;

const STORY_AUTHOR_MAP = config.STORY_AUTHOR_MAP;
const NUM_STORIES = config.NUM_STORIES;

const imagePathMap = {
  get: function(name){
    return "hacker.jpg"
  }
}


function App() {
 
  const [hackerNewsList, setHackerNewsList] = useState([]);
  const [pending, setPending] = useState(true);
  const [error, setError] = useState(null);
  const [counter,setCounter] = useState(0);

  function setAuthorsToTheirStories(authorList, storyList, storyAuthorFieldMap, imagePathMap, handleClickCallback){
    if(authorList!=null) {
      return storyList.map(st => {
        let au = authorList.find(a => st.by === a.id);
        st["authId"] = au[storyAuthorFieldMap["authId"]];
        st["authKarmaScore"] = au[storyAuthorFieldMap["authKarmaScore"]];
        st["imagePath"] = imagePathMap.get(st.id);
        st["onClickCallback"] = handleClickCallback;
        return st; 
    
      })
    }
    else {
      return storyList.map(st => {
       
        st["authId"] = config.AUTHOR_NOT_FOUND;
        st["authKarmaScore"] = config.KARMA_NOT_FOUND;
        st["imagePath"] = imagePathMap.get(st.id);
        st["onClickCallback"] = handleClickCallback;
        return st; 
    
      })
    }
  }
  
  function handleClick(e){   
    setCounter(counter => counter+1)
  }

  function setErrorMsg(msg){
    setPending(pending => !pending);
    setError(msg);
  }

  function setNewsList(list){
    setPending(pending => !pending);
    setHackerNewsList(list);
  }

  useEffect(() => {
    fetch(TOP_STORIES_URL)
    .then(res => res.json())
    .then(topNewsArr => {
      let slicedTopStories = topNewsArr.slice(0, NUM_STORIES);

      Promise.all(slicedTopStories.map(storyId => fetch(
        `${STORY_URL}/${storyId}.json?print=pretty`
       
      )))
      .then(resp => Promise.all( resp.map(r => r.json()) ))
      .then(storiesArr => {
        
        Promise.all(storiesArr.map(sId => fetch(
          `${AUTHOR_URL}/${sId.by}.json?print=pretty`
         
        )))
        .then(resp => Promise.all( resp.map(r => r.json()) ))
        .then(authorList => {
         
          const finalStories = setAuthorsToTheirStories(authorList, storiesArr, STORY_AUTHOR_MAP, imagePathMap, handleClick)
                          .sort((a,b) => b.score - a.score);
        
          setNewsList(finalStories);
          
        }).catch(err => {
          const finalStories = setAuthorsToTheirStories(null, storiesArr, STORY_AUTHOR_MAP, imagePathMap, handleClick)
                          .sort((a,b) => b.score - a.score);
         
          setNewsList(finalStories)
        });        
      }).catch(err => {
       
        setErrorMsg(config.STORY_URL_ERR.concat(STORY_URL))
      })    
    }).catch(err => {
      
      setErrorMsg(config.TOP_STORIES_ERR)
   
    })
  }
, [])

  return (
    <div className={classes.appBody}>

      <h1>Hacker News</h1>

        {!pending && hackerNewsList.length>0 ? <h3 className={classes.likes}>Likes: {counter}</h3>:<p></p>}
      
        {!pending && hackerNewsList.length>0 && <NewsItemList {...{hackerNewsList : hackerNewsList}}/>}
      
        {pending && <h2>Loading Top Hacker News. Please wait....</h2>}

        {!pending && error && <h2 className={classes.error}>{error}</h2>}
    
    </div>
  );
}

export default App;
