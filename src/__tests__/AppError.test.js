import {render, screen, waitFor,cleanup,fireEvent} from '@testing-library/react';
import App from '../App';
import config from "../configs/NewsConfig";

const TOP_STORIES_URL = config.TOP_STORIES_URL; 
const STORY_URL = config.STORY_URL; 
const AUTHOR_URL = config.AUTHOR_URL;

async function mockFetchTopUrlError(url) {
  switch (url) {
    case TOP_STORIES_URL: {
     
      const user = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
      return Promise.reject({
        ok: true,
        status: 200,
        json: async () => (user),
      })
    }
    //The actual line in this case is `${STORY_URL}/1.json?print=pretty`
    case (url.match(/item/) || {}).input: {
     
        return Promise.resolve({
          ok: true,
          status: 200,
          json: async () => ({
            id: (Math.random() + 1).toString(36).substring(7),
            title: 'Title1',
            url:'ftpgooglecom',
            time: 10000,
            score:123,
            by: 5
          })
        })
      }
      case `${AUTHOR_URL}/5.json?print=pretty`: {
   
        return Promise.resolve({
          ok: true,
          status: 200,
          json: async () => ({
            id:5,
            karma:321
          })
        })
      }     
  }
}

async function mockFetchStoryError(url) {
    switch (url) {
      case TOP_STORIES_URL: {
       
        const user = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
        return Promise.resolve({
          ok: true,
          status: 200,
          json: async () => (user),
        })
      }
      //The actual line in this case is `${STORY_URL}/1.json?print=pretty`
      case (url.match(/item/) || {}).input: {
       
          return Promise.reject({
            ok: true,
            status: 200,
            json: async () => ({
              id: (Math.random() + 1).toString(36).substring(7),
              title: 'Title1',
              url:'ftpgooglecom',
              time: 10000,
              score:123,
              by: 5
            })
          })
        }
        case `${AUTHOR_URL}/5.json?print=pretty`: {
     
          return Promise.resolve({
            ok: true,
            status: 200,
            json: async () => ({
              id:5,
              karma:321
            })
          })
        }     
    }
}


async function mockFetchAuthroError(url) {
    switch (url) {
      case TOP_STORIES_URL: {
       
        const user = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
        return Promise.resolve({
          ok: true,
          status: 200,
          json: async () => (user),
        })
      }
      //The actual line in this case is `${STORY_URL}/1.json?print=pretty`
      case (url.match(/item/) || {}).input: {
       
          return Promise.resolve({
            ok: true,
            status: 200,
            json: async () => ({
              id: (Math.random() + 1).toString(36).substring(7),
              title: 'Title1',
              url:'ftpgooglecom',
              time: 10000,
              score:123,
              by: 5
            })
          })
        }
        case `${AUTHOR_URL}/5.json?print=pretty`: {
     
          return Promise.reject({
            ok: true,
            status: 200,
            json: async () => ({
              id:5,
              karma:321
            })
          })
        }     
    }
  }

beforeAll(() => jest.spyOn(window, 'fetch'));
afterEach(() => cleanup());
afterAll(()=> jest.clearAllMocks());


test('renders error if top story upload fails', async () => {
    window.fetch.mockImplementation(mockFetchTopUrlError);
    const {rerender} = render(<App />);

    await waitFor(() => {
        expect(screen.getByText(config.TOP_STORIES_ERR)).toBeInTheDocument();
    },{timeout:300})
 
});
test('renders error if story upload fails', async () => {
    window.fetch.mockImplementation(mockFetchStoryError);
    const {rerender} = render(<App />);

    await waitFor(() => {
    
        const errs = screen.getAllByText(new RegExp(config.STORY_URL_ERR.slice(0,config.STORY_URL_ERR.length-1)[0],"i"));
        expect(errs.length > 1).toBeTruthy();

    },{timeout:300})
 
});
test('renders learn react link with mock fetch and click', async () => {
    window.fetch.mockImplementation(mockFetchAuthroError);
    const {rerender} = render(<App />);

    await waitFor(() => {  
       
        const titles = screen.getAllByText(new RegExp(config.AUTHOR_NOT_FOUND,"i")); 
        expect(titles.length === 2*config.NUM_STORIES).toBeTruthy();

    },{timeout:300});
 
});


