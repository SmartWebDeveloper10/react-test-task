
import { render, screen, waitFor } from '@testing-library/react';
import App from '../App';
import config from "../configs/NewsConfig";


test('Renders learn react link with live fetch. REQUIRES AN ACTIVE INTERNET CONNECTION TO RUN', async () => {
 
    const {rerender} = render(<App />);
  
    await waitFor(() => {
    
      const urls = screen.getAllByText(/https/i);
      const authors = screen.getAllByText(/author/i);
  
     expect(urls.length > 0).toBeTruthy();
     expect(authors.length === config.NUM_STORIES).toBeTruthy();
  
    },{timeout:10000});   
   
  });