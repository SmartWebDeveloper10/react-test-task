import React from 'react'
import NewsItem from './newsItem/NewsItem'

export default function NewsItemList(props) {

    const {hackerNewsList} = props;

    return (
        <>
            {hackerNewsList.map((hn) => {return ( <NewsItem key = {hn.id} {...hn}/>)})}
        </>
    )
}
