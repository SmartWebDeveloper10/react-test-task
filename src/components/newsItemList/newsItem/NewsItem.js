import React from 'react'
import classes from './NewsItem.module.css';

export default function NewsItem(props) {
  const {
    title,
    url,
    time,
    score,
    authId,
    authKarmaScore,
    imagePath,
    onClickCallback
  } = props

  return (
    <div className={classes.newsItem}>

      <div className={classes.header}>
        <h1>
          {title}
        </h1>
      </div>

      <div className={classes.body}>
        <p>
          URL: <a href={url} >{url}</a>
        </p>
        <p>
          Time: {new Date(time).toLocaleDateString("en-US")}
        </p>
        <p>
          Score: {score}
        </p>
      </div>

      <div className={classes.pic}>
        <img src={`/${imagePath}`} alt=""/>
      </div>

      <div className={classes.footer}>
        <h4>
          About the author:
        </h4>
        <p>
          {authId}
        </p>
        <p>
          Karma:  {authKarmaScore}
        </p>
        <div>
          <button onClick = {onClickCallback}>
            I like it!
          </button>
        </div>
      </div>

    </div>
  )
}