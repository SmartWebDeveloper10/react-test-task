const confs = {
    TOP_STORIES_URL : ' https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty',
    STORY_URL : ' https://hacker-news.firebaseio.com/v0/item',
    AUTHOR_URL : ' https://hacker-news.firebaseio.com/v0/user',
    STORY_AUTHOR_MAP : {
        'authId':'id',
        'authKarmaScore':'karma',
      
      },
    NUM_STORIES : 10,
    TOP_STORIES_ERR: "Error: No story Ids were uploaded! Check internet connection.",
    STORY_URL_ERR: "Error: No story details were uploaded! This site  may have moved somewhere: ",
    AUTHOR_NOT_FOUND: "Not Found",
    KARMA_NOT_FOUND: "Not Found"
}

export default confs;